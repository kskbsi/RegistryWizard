#!/bin/bash
set -o pipefail
set -e

[ -d "out/" ] && rm -r out/
mkdir -p out/classes/
javac -d out/classes/ -classpath "lib/*" $(find . -name '*.java')
cd out/classes/
find ../../lib/ -name \*.jar -type f -exec jar xvf {} \;
cd ../../
jar cvmf src/META-INF/MANIFEST.MF out/RegistryWizard.jar -C out/classes .
