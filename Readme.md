# RegistryWizard
A collection of tools to process and interact with Registry Data. Expanded as required by the Kioubit Network. 

## Current Features
1) Compute the **hierarchical relationship between IP prefixes** and output to json.
  This is useful for applications such as IP space visualization.
2) Output metadata of all inetnum and inet6num objects to json
3) **Generate Route Origin Authorization (ROA) data** for use with Bird 1/2 or for use with RPKI software
4) Generate a list of authentication methods for every ASN (Only available in the custom release)

### Example Output for 1.
```
{
"prefix": "0.0.0.0/0",
"children": [
    {
    "prefix": "172.20.0.0/14",
    "children": [
        {
        "prefix": "172.21.0.0/16",
        "children": [
            {
            "prefix": "172.21.191.0/24",
            "children": [
                {
                "prefix": "172.21.191.0/26",

...
```
### Example Output for 2.
```
{
  "10.24.32.0/19": {
    "inetnum": "10.24.32.0 - 10.24.63.255",
    "cidr": "10.24.32.0/19",
    "netname": "ICVPN-SLFL",
    "remarks": "File: slfl",
    "mnt-by": "DN42-MNT",
    "source": "ICVPN"
  },
...
```

### Example Output for 3.
```
route 10.127.22.0/29 max 29 as 4201270011;
route 172.20.193.176/28 max 29 as 4242423335;
route 172.23.192.0/24 max 29 as 64712;
route 10.24.32.0/19 max 24 as 64901;
route 172.21.75.64/26 max 29 as 141011;
route 172.20.60.0/23 max 29 as 4242423158;
...
```

### Example Output for 4.
```
{
"17080": [
{
"v4": [
{
"0": "172.20.148.128/27"
}
],
"v6": [
{
"0": "fd66:b99:c0da::/48"
}
],
"authmethods": [
{
"0": {
"type": "ssh-ed25519",
"category": "SSH",
"value": "AAAAC3NzaC1lZDI1NTE5AAAAI.. REDACTED ..7LjKezP61p08GMUM"
}
},
{
"1": {
"type": "email",
"category": "email",
"value": "redacted@redacted.com"
}
}
],
"MNT": "R-MNT"
}
],
...
```
