package dn42.kioubit.RegistryWizard;

import dn42.kioubit.RegistryWizard.modules.HierarchicalPrefixList;
import dn42.kioubit.RegistryWizard.modules.InetnumMetadata;
import dn42.kioubit.RegistryWizard.modules.RoaGenerator;

import java.io.File;

public class Main {
    public static void main(String[] args) {

        try {

            File RegistryDir = new File(args[0]);
            if (!RegistryDir.exists()) {
                System.err.println("Directory does not exist");
                return;
            }

            if (args[1].equals("inetnumMetadata")) {
                boolean Ignore10_8;
                File registry;
                boolean v6 = !args[2].equals("v4");
                if (v6) {
                    Ignore10_8 = false;
                    registry = new File(RegistryDir + "/data/inet6num/");
                } else {
                    Ignore10_8 = !args[3].equals("keep");
                    registry = new File(RegistryDir + "/data/inetnum/");
                }
                InetnumMetadata.output(registry, Ignore10_8);
                return;
            }

            if (args[1].equals("hierarchicalPrefixes")) {
                boolean Ignore10_8 = true;
                File registry;
                boolean v6 = !args[2].equals("v4");
                if (v6) {
                    registry = new File(RegistryDir + "/data/inet6num/");
                } else {
                    Ignore10_8 = !args[3].equals("keep");
                    registry = new File(RegistryDir + "/data/inetnum/");
                }
                HierarchicalPrefixList.output(registry, Ignore10_8);
                return;
            }

            if (args[1].equals("roa")){
                boolean bird2 = !args[2].equals("bird1");
                boolean json = args[2].equals("json");
                boolean v6 = !args[3].equals("v4");
                RoaGenerator.output(RegistryDir,bird2,v6,json);
                return;
            }

            if (args[1].equals("authmethods")){
                System.err.println("Only available in the custom release");
                return;
            }

            PrintUsage();

        } catch (IndexOutOfBoundsException ignored) {
            PrintUsage();
        }

    }
    private static void PrintUsage(){
        System.out.println("RegistryWizard Version 1.2.3-public");
        System.out.println("Usage: java -jar RegistryWizard.jar <path to root folder of registry> <module> <module args>");
        System.out.println("Available Modules:");
        System.out.println("-> inetnumMetadata <v4/v6> <(only for v4 :) keep/ignore-10_8>");
        System.out.println("-> hierarchicalPrefixes <v4/v6> <(only for v4 :) keep/ignore-10_8>");
        System.out.println("-> roa <bird2/bird1/json> <v4/v6/json46>");
        System.out.println("-> authmethods");
    }
}
