package dn42.kioubit.RegistryWizard.modules;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dn42.kioubit.RegistryWizard.utils.IPAddress;
import dn42.kioubit.RegistryWizard.utils.RouteObject;
import dn42.kioubit.RegistryWizard.utils.util;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RoaGenerator {
    private static Map<String, FilterSet> FilterV4;
    private static Map<String, FilterSet> FilterV6;

    public static void output(File registry, boolean bird2, boolean v6, boolean json) {
        parseFilterTxt(registry);
        ProduceOutput(parseRegistry(registry), bird2, v6,json);
    }

    private static void ProduceOutput(ArrayList<RouteObject> list, boolean bird2, boolean v6, boolean json) {
        if (json) {
            JsonObject main = new JsonObject();
            JsonObject metadata = new JsonObject();
            metadata.addProperty("counts",list.size());
            metadata.addProperty("generated",System.currentTimeMillis()/1000);
            metadata.addProperty("valid",System.currentTimeMillis()/1000 + 700000);
            main.add("metadata",metadata);

            JsonArray ROAs = new JsonArray();
            for (RouteObject item : list) {
                for (String origin : item.origin) {
                    JsonObject singleRoa = new JsonObject();
                    singleRoa.addProperty("prefix",item.route);
                    singleRoa.addProperty("maxLength",item.maxLen);
                    singleRoa.addProperty("asn","AS" + origin);
                    ROAs.add(singleRoa);
                }
            }
            main.add("roas",ROAs);
            System.out.println(main);
        } else {
            System.out.println("# " + RoaGenerator.class.getName());
            System.out.println("# ROA Last updated: " + Instant.now().toString());
            System.out.println("#");
            for (RouteObject item : list) {
                if (item.isV6 && !v6) {
                    continue;
                }
                if (!item.isV6 && v6) {
                    continue;
                }
                for (String origin : item.origin) {
                    StringBuilder sb = new StringBuilder();
                    if (bird2) {
                        sb.append("route ");
                    } else {
                        sb.append("roa ");
                    }
                    sb.append(item.route);
                    sb.append(" max ").append(item.maxLen).append(" as ").append(origin).append(";");
                    System.out.println(sb);
                }
            }
        }

    }


    private static ArrayList<RouteObject> parseRegistry(File registry) {
        File RegistryRoute4 = new File(registry + "/data/route/");
        File RegistryRoute6 = new File(registry + "/data/route6/");

        String[] PathNames4;
        String[] PathNames6;
        PathNames4 = RegistryRoute4.list();
        PathNames6 = RegistryRoute6.list();
        assert PathNames4 != null;
        assert PathNames6 != null;
        String[] PathNames = new String[PathNames4.length + PathNames6.length];
        System.arraycopy(PathNames4, 0, PathNames, 0, PathNames4.length);
        System.arraycopy(PathNames6, 0, PathNames, PathNames4.length, PathNames6.length);
        ArrayList<RouteObject> routeList = new ArrayList<>();


        for (String object : PathNames) {
            File testObject;
            boolean isV6 = false;
            ArrayList<String> origin = new ArrayList<>();
            boolean SeenMaxLen = false;
            boolean SeenOrigin = false;
            int maxLen = 0;
            String route = object.replace("_", "/");
            String IPCheck_tmp = route.split("/")[0];

            if (!IPAddress.isValidIP(IPCheck_tmp)) {
                System.err.println("ERROR: Read an invalid IP address while computing ROA " + IPCheck_tmp);
                continue;
            }
            if (IPAddress.isIPv6(IPCheck_tmp)) {
                isV6 = true;
            }

            if (isV6) {
                testObject = new File(RegistryRoute6 + "/" + object);
            } else {
                testObject = new File(RegistryRoute4 + "/" + object);
            }

            try {

                Scanner objectReader = util.ScanFile(testObject);
                while (objectReader.hasNextLine()) {
                    String data = objectReader.nextLine();
                    String[] KeyOb = data.split(": ",2);
                    if (KeyOb[0].equals("origin")) {
                        SeenOrigin = true;
                        origin.add(KeyOb[1].trim().replaceAll("[^\\d.]", ""));
                    }
                    if (KeyOb[0].equals("max-length")) {
                        SeenMaxLen = true;
                        maxLen = Integer.parseInt(KeyOb[1].trim());
                    }
                }
                objectReader.close();
            } catch (Exception e) {
                System.err.println("Error reading file for ROA: " + e.getMessage());
            }

            FilterSet filterSet = getDefaultLen(route);
            if (!filterSet.permitAction) {
                continue;
            }

            if (!SeenMaxLen) {
                maxLen = filterSet.MaxLength;
            }

            if (!SeenOrigin) {
                origin.add("0");
            }

            int RouteLength = Integer.parseInt(route.split("/")[1]);
            if (RouteLength < filterSet.MinLength) {
                maxLen = filterSet.MaxLength;
            }
            if (RouteLength > filterSet.MaxLength){
                continue;
            }

            if (RouteLength > maxLen) {
                continue;
            }

            if (maxLen < filterSet.MinLength || maxLen > filterSet.MaxLength){
                maxLen = filterSet.MaxLength;
            }

            RouteObject routeObject = new RouteObject(isV6, route, origin, maxLen);
            routeList.add(routeObject);
        }
        return routeList;
    }

    private static FilterSet getDefaultLen(String route) {
        String ip = route.split("/")[0];

        Map<String, FilterSet> FilterSet;
        if (IPAddress.isIPv6(ip)) {
            FilterSet = FilterV6;
        } else {
            FilterSet = FilterV4;
        }

        int MaxCompareLength = -1;
        FilterSet curFilterSet = null;
        for (Map.Entry<String, FilterSet> entry : FilterSet.entrySet()) {
            int EntryLength = Integer.parseInt(entry.getKey().split("/")[1]);
            if (EntryLength > MaxCompareLength) {
                if (IPAddress.isIpInSubnet(ip, entry.getKey())) {
                    MaxCompareLength = EntryLength;
                    curFilterSet = entry.getValue();
                }
            }
        }
        if (curFilterSet == null) {
            System.err.println("An error occurred");
        }
        return curFilterSet;
    }


    private static void parseFilterTxt(File registry){
        try {
            File v4 = new File(registry + "/data/filter.txt");
            FilterV4 = scanFilterTxt(v4);
            File v6 = new File(registry + "/data/filter6.txt");
            FilterV6 = scanFilterTxt(v6);
        } catch (Exception ex) {
            System.err.println("An error occurred");
            ex.printStackTrace();
        }
    }

    private static Map<String, FilterSet> scanFilterTxt(File toRead) throws Exception {
        Map<String, FilterSet> ReadFilter;
        ReadFilter = new HashMap<>();
        Scanner objScanner = util.ScanFile(toRead);
        while (objScanner.hasNextLine()){
            String data = objScanner.nextLine();
            if (data.startsWith("#")) {continue;}
            data = data.replaceAll("\\p{javaSpaceChar}+"," ");
            if (data.equals(" ") || data.isEmpty()) {continue;}
            if (data.contains("#")) {
                data = data.split("#",2)[0];
            }
            String[] s = data.split(" ");
            if (s.length != 5) {continue;}
            FilterSet f = new FilterSet(s[1],s[3],s[4]);
            ReadFilter.put(s[2],f);
        }
        return ReadFilter;
    }


    private static class FilterSet {
        int MinLength;
        int MaxLength;
        boolean permitAction;

        public FilterSet(String permitAction, String minLength, String maxLength){
            this.MaxLength = Integer.parseInt(maxLength);
            this.MinLength = Integer.parseInt(minLength);
            this.permitAction = permitAction.equals("permit");
        }

    }

}