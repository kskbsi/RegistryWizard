package dn42.kioubit.RegistryWizard.modules;

import dn42.kioubit.RegistryWizard.utils.IPAddress;
import dn42.kioubit.RegistryWizard.utils.inetnum;

import java.io.File;
import java.util.ArrayList;

public class HierarchicalPrefixList {
    private static boolean Ignore10_8 = false;

    public static void output(File registry, boolean Ignore10_8_tmp) {
        Ignore10_8 = Ignore10_8_tmp;

        ArrayList<inetnum> ProcessedList = Compare(ReadRegistry(registry));

        for (inetnum processed : ProcessedList) {
            if (processed.prefix.equals("0.0.0.0") || processed.prefix.equals("::")) {
                RecursiveOutput(processed, false, false, true);
                break;
            }
        }
    }


    private static ArrayList<inetnum> ReadRegistry(File registry) {
        String[] PathNames;
        ArrayList<inetnum> inetNumList = new ArrayList<>();

        PathNames = registry.list();

        assert PathNames != null;
        for (String item : PathNames) {
            String[] iParts = item.split("_");
            inetnum raw = new inetnum(iParts[0], Integer.parseInt(iParts[1]));

            if (Ignore10_8) {
                if (IPAddress.isIpInSubnet(raw.prefix, "10.0.0.0/8")) {
                    continue;
                }
            }

            inetNumList.add(raw);
        }
        return inetNumList;
    }

    private static ArrayList<inetnum> Compare(ArrayList<inetnum> rawList) {
        for (inetnum item : rawList) {
            int MaxCompareLength = -1;
            inetnum parent = null;
            for (inetnum CompareItem : rawList) {
                if (item.getCidr().equals(CompareItem.getCidr())) { continue; }
                if (CompareItem.length > MaxCompareLength) {
                    if (IPAddress.isSubnetInSubnet(CompareItem.getCidr(), item.getCidr())) {
                        MaxCompareLength = CompareItem.length;
                        parent = CompareItem;
                    }
                }
            }

            if (parent != null) {
                parent.children.add(item);
            }

        }
        return rawList;
    }

    private static void RecursiveOutput(inetnum testObject, boolean FirstTimeOnLayerCarry, boolean LastTimeOnLayer, boolean SpecialEndCase) {

        if (!FirstTimeOnLayerCarry) {
            System.out.print("{ \"prefix\": \"" + testObject.getCidr() + "\",");
        } else {
            System.out.print("\"children\": [{ \"prefix\": \"" + testObject.getCidr() + "\",");
        }

        boolean FirstTimeOnLayer = true;
        int CountdownToLast = testObject.children.size();
        for (inetnum item : testObject.children) {
            CountdownToLast = CountdownToLast - 1;
            if (CountdownToLast == 0) {
                RecursiveOutput(item, FirstTimeOnLayer, true, false);
            } else {
                RecursiveOutput(item, FirstTimeOnLayer, false, false);
            }
            if (FirstTimeOnLayer) {
                FirstTimeOnLayer = false;
            }
        }

        if (!LastTimeOnLayer) {
            if (!SpecialEndCase) {
                System.out.print(" \"size\": " + testObject.size + "},");
            } else {
                System.out.print(" \"size\": " + testObject.size + "}");
            }
        } else {
            System.out.print(" \"size\": " + testObject.size + "}],");
        }
    }
}
