package dn42.kioubit.RegistryWizard.modules;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dn42.kioubit.RegistryWizard.utils.IPAddress;
import dn42.kioubit.RegistryWizard.utils.util;

import java.io.File;
import java.util.Scanner;

public class InetnumMetadata {
    public static void output(File registry, Boolean Ignore10_8) {
        JsonObject main = new JsonObject();

        String[] pathNames = registry.list();

        assert pathNames != null;
        for (String item : pathNames) {
            try {
                String inetNumCidr = item.replace("_","/");
                if (Ignore10_8) {
                    if (IPAddress.isSubnetInSubnet("10.0.0.0/8", inetNumCidr)) {
                        continue;
                    }
                }

                JsonArray SingleInetNum = new JsonArray();
                JsonObject SingleInetNumKeys = new JsonObject();
                File myObj = new File(registry + "/" + item);
                Scanner objectReader = util.ScanFile(myObj);
                while (objectReader.hasNextLine()) {
                    String data = objectReader.nextLine();
                    String[] keyObj = data.split(":" , 2);
                    SingleInetNumKeys.addProperty(keyObj[0].trim(),keyObj[1].trim());
                }
                objectReader.close();
                SingleInetNum.add(SingleInetNumKeys);
                main.add(inetNumCidr,SingleInetNumKeys);
            } catch (Exception e) {
                System.err.println("An error occurred.");
                e.printStackTrace();
            }
        }

        System.out.print(main);
    }
}
