package dn42.kioubit.RegistryWizard.utils;

import java.util.ArrayList;

public class RouteObject {
    public boolean isV6;
    public String route;
    public ArrayList<String> origin;
    public int maxLen;

    public RouteObject(boolean isV6, String route, ArrayList<String> origin, int maxLen) {
        this.isV6 = isV6;
        this.route = route;
        this.origin = origin;
        this.maxLen = maxLen;
    }
}
