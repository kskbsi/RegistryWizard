package dn42.kioubit.RegistryWizard.utils;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class util {
    /**
     * Checks if given byte array is valid UTF-8 encoded.
     *
     * @param bytes Byte Array
     * @return true when valid UTF8 encoded
     */
    public static boolean isValidUTF8(final byte[] bytes) {
        try {
            Charset.availableCharsets().get("UTF-8").newDecoder().decode(ByteBuffer.wrap(bytes));

        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }

    public static Scanner ScanFile(File file) throws Exception {
        byte[] tmp = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
        if (!util.isValidUTF8(tmp)) { throw new Exception("File is not UTF-8"); }
        return new Scanner(new String(tmp));
    }

}
