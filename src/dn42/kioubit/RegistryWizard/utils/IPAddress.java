package dn42.kioubit.RegistryWizard.utils;

import com.github.jgonian.ipmath.Ipv4;
import com.github.jgonian.ipmath.Ipv4Range;
import com.github.jgonian.ipmath.Ipv6;
import com.github.jgonian.ipmath.Ipv6Range;
import org.apache.commons.validator.routines.InetAddressValidator;


public class IPAddress {

    /**
     * Check if a smaller Subnet is within a lager Subnet defined by CIDR value
     * @param  BigCidr The bigger Subnet
     * @param  SmallCidr The smaller Subnet
     * @return Whether the smaller Subnet fits into the bigger Subnet
     */
    public static boolean isSubnetInSubnet(final String BigCidr, final String SmallCidr){
        if (isIPv6(SmallCidr.split("/")[0])) {
            return Ipv6Range.parseCidr(BigCidr).contains(Ipv6Range.parseCidr(SmallCidr));
        }
        return Ipv4Range.parseCidr(BigCidr).contains(Ipv4Range.parseCidr(SmallCidr));
    }

    /**
     * Check if IP is within a Subnet defined by the Subnet CIDR notation
     * @param  ip The IP Address to check
     * @param  SubnetCidr The subnet in the CIDR notation
     * @return Whether the ip is within the subnet
     */
    public static boolean isIpInSubnet(String ip, String SubnetCidr) {
        try {
            if(isIPv6(ip)) {
                if (Ipv6Range.parseCidr(SubnetCidr).contains(Ipv6.parse(ip))){
                    return true;
                }
            } else {
                if (Ipv4Range.parseCidr(SubnetCidr).contains(Ipv4.parse(ip))){
                    return true;
                }
            }

        } catch(Exception ignored) { }
        return false;
    }

    /**
     * Check if IP is valid
     * @param  ip The IP Address to check
     * @return Whether the ip is valid
     */
    public static boolean isValidIP(final String ip){
        try {
            if (ip == null || ip.length() < 2) {
                return false;
            }
            InetAddressValidator validator = InetAddressValidator.getInstance();
            if (validator.isValid(ip)){
                return true;
            }
        }
        catch (Exception ex) {
            return false;
        }
        return false;
    }

    /**
     * Check if an IP is v6
     * @param  ip The IP Address to check
     * @return Whether the ip is v6
     */
    public static boolean isIPv6(final String ip){
        try {
            if (ip == null || ip.length() < 2) {
                return false;
            }
            InetAddressValidator validator = InetAddressValidator.getInstance();
            if (validator.isValidInet6Address(ip)){
                return true;
            }
        }
        catch (Exception ex) {
            return false;
        }
        return false;
    }

}