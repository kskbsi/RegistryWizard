package dn42.kioubit.RegistryWizard.utils;

import com.github.jgonian.ipmath.Ipv4Range;
import com.github.jgonian.ipmath.Ipv6Range;

import java.math.BigInteger;
import java.util.ArrayList;

public class inetnum {
    public String prefix;
    public int length;
    public BigInteger size;
    public ArrayList<inetnum> children;

    public inetnum(String prefix, int length) {
        this.length = length;
        this.prefix = prefix;
        this.size = CalcSize(prefix, length);
        children = new ArrayList<>();
    }

    private static BigInteger CalcSize(String prefix, int length){
        if (IPAddress.isIPv6(prefix)) {
            return Ipv6Range.parse(prefix + "/" + length).size();
        }
        return BigInteger.valueOf(Ipv4Range.parse(prefix + "/" + length).size());
    }

    public String getCidr(){
        return prefix + "/" + length;
    }

}